var gulp = require('gulp');
var minify = require('./node_modules/gulp-minify');
var concat = require('./node_modules/gulp-concat');
var minifyCSS = require('./node_modules/gulp-clean-css');

gulp.task('default', function() {
  
  gulp.src([
    './app/js/vendor/angular.min.js',
    './app/js/vendor/angular-ui-router.min.js',
  ])
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./outputs'))
    .pipe(minify({
      ext:{
	min:'.min.js'
      },
      noSource:true
    }))
    .pipe(gulp.dest('./public'));

  gulp.src([
    './app/js/app.js',
    './app/js/controller.js',
    './app/js/config.js'
  ])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('./outputs'))
    .pipe(minify({
      ext:{
	min:'.min.js'
      },
      noSource:true
    }))
    .pipe(gulp.dest('./public'));

  gulp.src([
    './app/css/bootstrap.min.css',
  ])
    .pipe(concat('vendor.min.css'))
    .pipe(gulp.dest('./outputs'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./public'));

  gulp.src([
    './app/css/custom.css'
  ])
    .pipe(concat('app.min.css'))
    .pipe(gulp.dest('./outputs'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./public'));
});
