# README #
School Smart Web Server

## What is this repository for? ##
cloud and app solution that addresses safety and traffic management associated with student commuting.
It is primarily targeted at year 2 – 8 students who have some limited independence in their commute to
 and from school.

### Summary of set up ###
Use npm to install all dependencies and npm start to start the server  

### Dev enviroment ###
grunt watch must be running before editing files in the /template dir (and others)
npm install (install dependencies)
node server.js (runs the server)
open index.html

### Dependencies ###
see package.json

### Database configuration ###
Will have to sort this out, including a way of getting testing data to
each others' machines.  

### How to run tests ###
For now, no way to run tests..  

Need to do this.. probably add another section to gulpfile that runs a
series of tests. Need to add this and a folder that includes the tests  

## Deployment instructions ##
firebase login
firebase init
firebase deplay

### Who do I talk to? ###
Jordan Bettonvil, Callum Macoun, James Eggington and Demetrios Christou  
-Pseudocoders Team.

issue tracker: https://schoolsmart.myjetbrains.com/youtrack/issues
